// console.log("hello");


let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

function enterUser(varA){
	// let user = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
	varA[varA.length] = "John Cena";
	// console.log(user);
}

enterUser (users);
console.log(users);



/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

function enterUser1(index){
	// let user1 = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
	return users[2];

}
enterUser1(users);


// let itemfound = varC;
console.log(enterUser1(2));



/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/


function getLastUser(varD) {
    let lastItem = varD[varD.length-1];
    return lastItem
}

getLastUser(users);
console.log(getLastUser(users))

/*function enterUser2(varD){
    // let user2 = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista","John Cena"];
    return varE = varD[varD.length-1];
}
enterUser2(users);

let lastUser = varE;
console.log(lastUser);
*/




/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

function updateItem(update, index){

    let secondUpdate = update;
    secondUpdate.length = secondUpdate.length-1
    console.log(secondUpdate);
    
    secondUpdate.length = secondUpdate.length - 1;
    secondUpdate[3] = "Triple H"
    console.log(secondUpdate)
    
    /*for(let i = 0; i < secondArray.length; i++){
        array[array.length] = secondArray[i];
        console.log(array);
        
    }*/

    
}

updateItem(users, 4);

/*function updateItem(){
	let update = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista","John Cena"];
	 update.length = update.length-1
	 console.log (update)
	 update[update.length] = "Triple H";
	 console.log(update)

}

updateItem()
*/


/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

function delUsers(){
	let deleteUser = []
	console.log(deleteUser);
}

delUsers();




/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
function checkUserLength(){
	if (delUsers.length > 0){
		return false
	}
	else{
		return true
	}
}
checkUserLength()

let isUsersEmpty = checkUserLength();
console.log(isUsersEmpty);